def arithmetic(arg1,arg2,operation):

    """
    Функция arithmetic.

    Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними.
    Если третий аргумент +, сложить их;
    если —, то вычесть;
    если *, то умножить;
    если /, то разделить (первое на второе).
    В остальных случаях вернуть строку "Unknown operator".
    Вернуть результат операции.
    """
    ...
    if operation =='+':
        return arg1 + arg2

    elif operation == '-':
        return arg1 - arg2

    elif operation == '*':
        return arg1 * arg2

    elif operation == '/':
        return arg1 / arg2
    else:
        return "Unknown operator"


