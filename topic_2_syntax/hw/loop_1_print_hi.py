def print_hi(n):
    """
    Функция print_hi.

    Принимает число n.
    Выведите на экран n раз фразу "Hi, friend!"
    Если число <= 0, тогда вывести пустую строку.
    Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"
    """
    ...
    print(n * "Hi, friend!")


