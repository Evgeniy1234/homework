def check_substr(str1, str2):
    """
    Функция check_substr.
    
    Принимает две строки.
    Если меньшая по длине строка содержится в большей, то возвращает True,
    иначе False.
    Если строки равны, то False.
    Если одна из строк пустая, то True.
    """
    if len(str1) == len(str2):
        return False
    elif len(str1) < len(str2) and str1 in str2:
        return True
    elif len(str1) > len(str2) and str2 in str1:
        return True
    else:
        return False
