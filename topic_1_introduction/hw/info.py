"""
Ввод данных с клавиатуры:
- Имя
- Фамилия
- Возраст

Вывод данных в формате:
"Вас зовут <Имя> <Фамилия>! Ваш возраст равен <Возраст>."
"""
name = input('Your name: ')
your_last_name = input('Your last name: ')
age = input('your age: ')

print("Вас зовут ", name,
      ",", your_last_name,
      "! Ваш возраст равен'", age, ".", sep="")

# альтенативный способ f-string
print(f"Вас зовут' {name}' {your_last_name}! Ваш возраст равен {age}.")
